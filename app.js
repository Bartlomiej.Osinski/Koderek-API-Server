var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var users = require('./routes/users');
var openings = require('./routes/openings');

var app = express();
var cors = require('cors');

app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json({ limit: '20kb' }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use((req, res, next) => {
  req.token = req.get('x-koderek-token');
  next();
});
app.use('/', index);
app.use('/users', users);
app.use('/openings', openings);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  err.message = 'Could not find resources.';
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.json({ msg: err.message });
});

module.exports = app;
