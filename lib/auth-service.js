let jsonfile = require('jsonfile');
const cwd = process.cwd();
const path = require('path');
const randToken = require('rand-token');
class User {
    constructor (user) {
        Object.assign(this, user)
    }
    exists () {
        return findByEmail(this._getUsers(), this.email);
    }
    create (user) {
        if (this.exists()) {
            return undefined;
        }
        else {
            this.userId = randToken.generate(16);
            this.userRole = user ? user.userRole : this.userRole;
            this.firstname = user ? user.firstname : this.firstname;
            this.lastname = user ? user.lastname : this.lastname;
            this.email = user ? user.email : this.email;
            this.pass = user ? user.pass : this.pass;
            let userSchema = JSON.parse(JSON.stringify(this));
            let users = this._getUsers();
            delete userSchema.userId;
            users.push(userSchema);
            jsonfile.writeFileSync(path.join(cwd, 'db', 'users.json'), users);
            return this;
        }
    }
    createSession (token) {
        this.userId = token;
        let sessions = this._getSessions();
        sessions[token] = this.email;
        jsonfile.writeFileSync(path.join(cwd, 'db', 'sessions.json'), sessions);
    }
    _getUsers () {
        return jsonfile.readFileSync(path.join(cwd, 'db', 'users.json'));
    }
    _getSessions () {
        return jsonfile.readFileSync(path.join(cwd, 'db', 'sessions.json'));
    }
}
let auth = {
    exists (token) {
        let sessions = jsonfile.readFileSync(path.join(cwd, 'db', 'sessions.json'));
        return sessions[token] || false;
    },
    login (credentials) {
        let user = new User(credentials);
        if (user.exists() && user.pass === user.exists().pass) {
            user.createSession(randToken.generate(16));
        }
        return user.userId;
    },
    register (userSession) {
        let user = new User(userSession);
        if (user.exists()) {
            return { status: 200, user };
        }
        else {
            // create
            user.create();
            return { status: 201, user };
        }

    },
    logout (token) {
        let sessions = jsonfile.readFileSync(path.join(cwd, 'db', 'sessions.json'));
        const removed = delete sessions[token];
        jsonfile.writeFileSync(path.join(cwd, 'db', 'sessions.json'), sessions);
        return removed;

    }
};
module.exports = auth;

function findByEmail (users, email) {
    return users.find( user => user.email === email) || null;
}