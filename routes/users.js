var express = require('express');
var router = express.Router();
let auth = require('../lib/auth-service');

/* GET users listing. */
router.post('/login', function(req, res, next) {
  let credentials = req.body.user;
  let token = auth.login(credentials);
  res.status(token ? 201 : 400);
  res.json({ token });
});
router.post('/register', function(req, res, next) {
    let userToCreate = req.body.user;
    let user = auth.register(userToCreate);
    res.status(user.status);
    res.json(user);
});


module.exports = router;
