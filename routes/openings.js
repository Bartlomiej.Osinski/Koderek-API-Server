const express = require('express');
const router = express.Router();
let auth = require('../lib/auth-service');
let jsonfile = require('jsonfile');
let path = require('path');
const cwd = process.cwd();
const randToken = require('rand-token');


/* GET users listing. */
router.get('/:id?', function(req, res, next) {
    let openings = jsonfile.readFileSync(path.join(cwd, 'db', 'openings.json'));
    const id = req.params.id;
    if (id) {
        let foundOpening = openings.find(item => item.id == id);
        res.json(foundOpening || {});
    }
    else {
        res.json(openings);
    }
});
router.post('/', function(req, res, next) {
    const token = req.token;
    if (token) {
        let opening = req.body.opening;
        let openings = jsonfile.readFileSync(path.join(cwd, 'db', 'openings.json'));
        opening.id = randToken.generate(16);
        openings.push(opening);
        jsonfile.writeFileSync(path.join(cwd, 'db', 'openings.json'), openings);
        res.status(201);
        res.json(opening);
    }
    else {
        res.status(403);
        res.json({ msg: 'provide token' });
    }
});
router.put('/:id', function(req, res, next) {
    const token = req.token;
    if (token) {
        const id = req.params.id;
        let opening = req.body.opening;
        opening.id = id;
        let openings = jsonfile.readFileSync(path.join(cwd, 'db', 'openings.json'));
        let openingIndex = openings.findIndex(item=>item.id==id);
        if (openingIndex > -1 && opening) {
            openings.splice(openingIndex, 1, opening);
            jsonfile.writeFileSync(path.join(cwd, 'db', 'openings.json'), openings);
            res.status(201);
            res.json(id && openingIndex !== -1 ? opening : {});
        }
        else {
            res.status(400);
            res.json({ msg: 'not found' });
        }
    }
    else {
        res.status(403);
        res.json({ msg: 'provide token' });
    }
});
router.delete('/:id', function(req, res, next) {
    const token = req.token;
    if (token) {
        const id = req.params.id;
        let opening = req.body.opening;
        let openings = jsonfile.readFileSync(path.join(cwd, 'db', 'openings.json'));
        let openingIndex = openings.findIndex(item=>item.id==id);
        openings.splice(openingIndex, 1);
        jsonfile.writeFileSync(path.join(cwd, 'db', 'openings.json'), openings);
        res.status(204);
        res.json(id || openingIndex === -1 ? opening : {});
    }
    else {
        res.status(403);
        res.json({ msg: 'provide token' });
    }
});


module.exports = router;
